//
//  array.hpp
//  CommandLineTool
//
//  Created by Joel on 16/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef array_hpp
#define array_hpp

class Array
{
public:

    Array();

    ~Array();

    void add (float itemValue);

    float get (int index) const;

    int size() const;
    
    void remove (int index);
    
    void reverse();
    
    static bool testArray();
    
private:
    int arraySize;
    float* data;
};

#endif /* array_hpp */
